module.exports = {
  'Demo test' : function (client) {
    client
      .url('http://localhost:8000')
      .waitForElementVisible('body', 1000)
      
      // 
      .assert.title('JEDi Signage')
      .assert.visible('input[type=text]#login')
      .setValue('input[type=text]', 'rembrandt van rijn')
      .waitForElementVisible('button[name=btnG]', 1000)
      .click('button[name=btnG]')
      .pause(1000)
      .assert.containsText('ol#rso li:first-child',
        'Rembrandt - Wikipedia')
      .end();
  }
};