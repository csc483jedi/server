let app = require('../../app');
let server;
let {db, User, ready} = require('../../models');

module.exports = {
  before: async (browser, done) => {
    await ready;
    
    // create test account
    let username = 'test';
    let password = 'password';
    
    let user = await User.create({
      username
    });
    
    await user.setPassword(password);
    await user.save();
    
    // Launch app
    server = app.listen(8000, done);
  },
  
  after: async (browser, done) => {
    // Shutdown app
    await db.close()
    
    server.close(done);
  },
  
  'Verify correct login flow': function(client) {
    let pfx = 'http://localhost:8000';
    
    client
    
      //  Test step 1
      // =============
      // Navigate to /
      .url(pfx)
      .waitForElementVisible('body', 1000)
      // Ensure we have been redirected correctly
      .assert.urlEquals(pfx + '/login')
      // Ensure the username, password prompts and login button are visible
      .assert.visible('input[type="text"][name="username"]')
      .assert.visible('input[type="password"][name="password"]')
      .assert.visible('button#login')
      .assert.containsText('#login', 'Login')
      
      //  Test step 2
      // =============
      
      // Enter an invalid username and password
      .setValue('[name="username"]', 'bad')
      .setValue('[name="password"]', 'bad')
      .click('#login')
      
      .waitForElementVisible('body', 1000)
      // Remain at /login
      .assert.urlEquals(pfx + '/login')
      // prompt displayed again
      .assert.visible('input[type="text"][name="username"]')
      .assert.visible('input[type="password"][name="password"]')
      .assert.visible('button#login')
      .assert.containsText('#login', 'Login')
      
      // Invalid login message visible
      .assert.visible('.alert-warning')
      .assert.containsText('.alert-warning', 'Invalid login')
      
      //  Test step 3
      // =============
      
      // Enter a valid username and password
      .setValue('[name="username"]', 'test')
      .setValue('[name="password"]', 'password')
      .click('#login')
      .waitForElementVisible('body', 1000)
      // Get a session cookie
      .getCookie('session', function(res) {
        console.assert(res, 'Did not get session cookie')
      })
      
      // Directed to /
      .assert.urlEquals(pfx + '/')

    
      .end();
  }
}