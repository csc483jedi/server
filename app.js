let express = require('express');
let logger = require('morgan');
let bodyParser = require('body-parser');
let cookieParser = require('cookie-parser');
let nunjucks = require('express-nunjucks');
let path = require('path');
let browserify = require('browserify-middleware');

let app = module.exports = express();

let isDev = app.get('env') === 'development';

// Set up middleware
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({  }));
app.use(cookieParser());

// Set up view engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'njk');
let njk = nunjucks(app, {
  //watch: isDev,
  noCache: isDev
})

// Static files
app.use(express.static(path.join(__dirname, 'public')));

// User-uploaded media
app.use('/media', express.static(path.join(__dirname, 'media')));

// Tie in our controllers
app.use(require('./controllers'));

// Generated front-end file
app.get('/', (req, res, next) => {
  res.sendFile(path.join(__dirname, 'vue/index.html'));
});
app.get('/app.js', browserify(path.join(__dirname, 'vue', 'main.js'), {
  debug: true,
  transform: ['vueify']
}))

// Catch-all route
app.use((req, res, next) => {
  let err = new Error('Not Found: ' + req.baseUrl)
  err.status = 404;
  next(err);
});

// Error handler
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  
  if (err.status !== 404)
    console.error(err.stack);
  
  if (app.get('env') === 'development') {
    res.send('<pre>' + err.stack);
  } else {
    res.send(err.message);
  }
})