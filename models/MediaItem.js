let sqlz = require('sequelize');
let fs = require('fs');
let Promise = require('bluebird');
Promise.promisifyAll(fs);
let path = require('path');

module.exports = function(models, db) {
  
  models.MediaItem = db.define('MediaItem', {
    id: {
      primaryKey: true,
      type: sqlz.INTEGER,
      autoIncrement: true
    },
    name: sqlz.STRING,
    originalName: sqlz.STRING,
    filepath: sqlz.STRING,
    
    // Current status
    // All start as pending, can be rejected on uploaded
    // Can be deleted or confirmed after upload
    // Can be deleted after confirming
    status: {
      type: sqlz.ENUM,
      values: ['pending', 'confirmed', 'rejected', 'deleted'],
      defaultValue: 'pending',
      get() {
        return this.getDataValue('status')
      }
    },
    
    // Image dimensions
    filesize: sqlz.INTEGER,
    width: sqlz.INTEGER,
    height: sqlz.INTEGER,
    
    // 0 indicates to use the global default
    defaultDuration: sqlz.INTEGER
  })
  
  models.MediaItem.prototype.confirm = async function() {
    let current = this.getDataValue('status');
    if (current !== 'pending') {
      throw new Error('Cannot confirm a non-pending MediaItem');
    }
    
    // Move from /temp to /media
    await fs.renameAsync(path.join('media/temp', this.filepath),
                         path.join('media',      this.filepath));
    
    // Confirm after successful move
    this.setDataValue('status', 'confirmed');
  }
  
  models.MediaItem.prototype.reject = async function() {
    let current = this.getDataValue('status');
    if (current !== 'pending') {
      throw new Error('Cannot confirm a non-pending MediaItem');
    }
    
    // Remove temporary file
    await fs.unlinkAsync(path.join('media/temp', this.filepath));
    
    this.setDataValue('status', 'rejected');
  }
  
  models.MediaItem.prototype.delete = async function() {
    let current = this.getDataValue('status');
    if (current === 'rejected' || current === 'deleted') {
      throw new Error('Cannot delete an already deleted MediaItem');
    }
    
    // Remove appropriate file
    if (current === 'pending')
      await fs.unlinkAsync(path.join('media/temp', this.filepath));
    else if (current === 'confirmed')
      await fs.unlinkAsync(path.join('media', this.filepath));
    else
      throw new Error('Invalid state ' + current + ' in MediaItem.delete');
    
    this.setDataValue('status', 'deleted');
  }
  
  models.MediaItem.prototype.toTreeInfo = function() {
    return {
      type: 'file',
      id: this.id,
      name: this.name,
      filename: this.originalName,
      parent: this.parentId,
      
      // Properties
      defaultDuration: this.defaultDuration
    }
  }
  
  models.MediaItem.prototype.delete = async function() {
    // Update status
    this.status = 'deleted';
    await this.save();
    
    // Unlink our file
    try {
      await fs.unlinkAsync(this.filepath);
    } catch(e) {
      if (e.code === 'ENOENT') {
        // Already gone, no worries
      } else {
        // Otherwise, complain
        console.error(e.stack);
        throw new Error('Failed to remove MediaItem file')
        
        return;
      }
    }
  }
  
  // Default schedule
  // overrides global default (always on)
  // overridden by image LayoutNode or PlaylistItem schedule
  models.MediaItem.hasOne(models.Schedule, { as: 'defaultSchedule' });
}