let sqlz = require('sequelize');
let Promise = require('bluebird');
let SSH = require('node-ssh');
let forge = require('node-forge');
let rsa = Promise.promisifyAll(forge.pki.rsa);
let nunjucks = require('nunjucks');
let path = require('path');

module.exports = function(models, db) {
  
  let Player = models.Player = db.define('Player', {
    id: {
      primaryKey: true,
      type: sqlz.INTEGER,
      autoIncrement: true
    },
    
    name: sqlz.STRING,
    hostname: sqlz.STRING,
    
    lastStatusTime: sqlz.DATE,
    lastStatus: {
      type: sqlz.ENUM,
      values: ['offline', 'sleep', 'active'],
      defaultValue: 'offline'
    },
    
    // For SSH connection
    privateKey: {
      type: sqlz.STRING,
      // Don't allow anyone to touch this without thinking
      set: function(data) { return undefined },
      get: function() { return undefined }
    },
    publicKey: {
      type: sqlz.STRING,
      // Don't allow this to be written manually
      set: function(data) { return undefined }
    }
  })
  
  Player.prototype.getPrivateKey = function() {
    return this.getDataValue('privateKey');
  }
  
  Player.prototype.checkStatus = async function() {
    // TODO XXX Move to config
    let minimumTimeSinceLast = 5 * 60;
    let now = Date.now() / 1000;
    let delta = now - this.lastStatusTime;
    
    if (delta > minimumTimeSinceLast) {
      
    } else {
      return this.lastStatus;
    }
  }
  
  Player.prototype.rekey = async function(bits = 2048) {
    let key = await rsa.generateKeyPairAsync({bits});
    
    this.setDataValue('privateKey', forge.ssh.privateKeyToOpenSSH(key.privateKey));
    this.setDataValue('publicKey', forge.ssh.publicKeyToOpenSSH(key.publicKey));
    await this.save();
  }
  
  Player.prototype.build = async function() {
    // Compile our layouts into some HTML
    // The big cheese!
    let out = await Promise.map(await this.getLayouts(), l => l.buildMetadata());

    return out;
  }
  
  Player.prototype.deploy = async function() {
    let session = new SSH();
  
    await session.connect({
      host: this.hostname,
      username: 'signage',
      privateKey: this.getPrivateKey()
    });
    
    let built = await this.build();
    
    // XXX we need error checking coming back from stderr streams of execCommand
    
    let body = nunjucks.render(path.join(__dirname, '../views/playerTemplate.njk'), {
      layouts: built
    })
    
    // Deploy media files
    let sent = [];
    for (let layout of built) {
      for (let file of layout.files) {
        if (sent.includes(file)) continue;
        
        // XXX check out those paths
        let from = path.join(__dirname, '../', file);
        let to = '/home/signage/http/' + file
        console.log('scp %s %s', from, to);
        let res = await session.putFile(from, to);
        console.log(res);
      }
    }

    // Deploy the lib files, just for fun
    let localLib = path.join(__dirname, '../public/playerlib');
    let remoteLib = '/home/signage/http/playerlib';
    await session.putDirectory(localLib, remoteLib)
    
    // Deploy index file
    await session.execCommand('cat>~/http/built.html', {
      stdin: body
    })
    
    // Trigger restart
    await session.execCommand('~/reload.sh');
  }
  
}