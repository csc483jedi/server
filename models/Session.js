let sqlz = require('sequelize');
let crypto = require('crypto');

module.exports = (models, db) => {

  models.Session = db.define('session', {
    token: {
      type: sqlz.STRING,
      get: function() { return this.getDataValue('token'); }
    }
  }, {
    hooks: {
      beforeCreate: (session, options) => {
        session.setDataValue('token', crypto.randomBytes(32).toString('hex'));
      }
    }
  });

}