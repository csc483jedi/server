let sqlz = require('sequelize');
let Promise = require('bluebird');

module.exports = function(models, db) {

  let LayoutNode = models.LayoutNode = db.define('LayoutNode', {
    id: {
      primaryKey: true,
      type: sqlz.INTEGER,
      autoIncrement: true
    },
    
    // Node ID, in our specific tree
    nid: sqlz.INTEGER,
    type: sqlz.STRING,

    name: sqlz.INTEGER
  }, {
    hooks: {
      beforeDestroy: async (instance, options) => {
        let {transaction} = options;
        let props = await instance.getProps({ transaction });
        for (let prop of props) {
          await prop.destroy({ transaction });
        }
      }
    }
  })

  LayoutNode.prototype.getProperty = async function(key) {
    return JSON.parse((await this.getProperty({
      where: { key }
    })).value);
  }

  LayoutNode.prototype.getProperties = async function() {
    let arr = await this.getProps();
    let obj = {};
    for (let item of arr)
      obj[item.key] = JSON.parse(item.value);
    return obj;
  }

  LayoutNode.prototype.toTreeInfo = async function() {
    let properties = await this.getProperties();
    let parent = await this.getParent();

    return {
      id: this.nid,
      name: this.name,
      typeName: this.type,
      parent: parent ? parent.nid : null,
      properties
    }
  }



  LayoutNode.hasMany(LayoutNode, { as: 'children' });
  LayoutNode.belongsTo(LayoutNode, { as: 'parent' });

  // Also this
  let LayoutNodeProperty = models.LayoutNodeProperty = db.define('LayoutNodeProperty', {
    id: {
      primaryKey: true,
      type: sqlz.INTEGER,
      autoIncrement: true
    },

    key: sqlz.STRING,
    value: sqlz.STRING
  });

  LayoutNode.Props = LayoutNode.hasMany(LayoutNodeProperty, { as: 'props' });
}