let sqlz = require('sequelize');

module.exports = function(models, db) {
  let PlayerLayout = models.PlayerLayout = db.define('PlayerLayout', {
    seqno: {
      type: sqlz.INTEGER,
      unique: 'key'
    },
    PlayerId: {
      type: sqlz.INTEGER,
      unique: 'key'
    },
    LayoutId: {
      type: sqlz.INTEGER
    }
  }, {
    hooks: {
      beforeCreate: async (lp, options) => {
        // Set seqno to max seqno + 1 from this PlayerId
        let highest = await PlayerLayout.findOne({
          where: { PlayerId: lp.PlayerId },
          order: [ ['seqno', 'DESC'] ]
        })

        let next = highest ? highest.seqno + 1 : 0;

        lp.seqno = next;
      }
    }
  })
}