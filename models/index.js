let sqlz = require('sequelize');

// Note, we can split these up later into a "models" folder
// We just have to have an index that ties them all back to this namespace

let db = new sqlz({
  dialect: 'sqlite',
  storage: 'db.db3',
  operatorsAliases: false,
  logging: false,
  define: {
    freezeTableName: true
  }
});

// Note, we have some ordered dependencies here

let models = module.exports = {db};

require('./Session')(models, db);
require('./User')(models, db);

require('./Schedule')(models, db);

require('./MediaItem')(models, db);
require('./MediaItemFolder')(models, db);

require('./LayoutNode')(models, db);
require('./Layout')(models, db);
models.LayoutNode.belongsTo(models.Layout, { as: 'rootLayout' });
models.Layout.hasMany(models.LayoutNode, { as: 'children' })
models.Layout.hasOne(models.LayoutNode, { as: 'root' });

require('./Player')(models, db);

require('./PlayerLayout')(models, db);
models.Player.belongsToMany(models.Layout, { through: 'PlayerLayout' });
models.Layout.belongsToMany(models.Player, { through: 'PlayerLayout' });


// XXX A minor hack, putting the ready point here
models.ready = db.sync({ force: false }).then(async () => {
  //await models.Layout.sync({ alter: true })
  //await models.LayoutNode.sync({ alter: true })
  
  // Test account if not existing
  let user = await models.User.find({ where: { username: 'admin' }});
  
  if (!user) {
    user = await models.User.create({
      username: 'admin'
    })
    await user.setPassword('password')
    await user.save();
  }
  
  // Root Media folder
  let MediaRoot = await models.MediaItemFolder.findOne({
    where: { name: 'root' }
  });
  
  if (!MediaRoot) {
    MediaRoot = await models.MediaItemFolder.create({
      id: 0,
      name: 'root'
    })
  }
  
});