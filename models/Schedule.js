let sqlz = require('sequelize');

module.exports = function(models, db) {
  
  models.Schedule = db.define('Schedule', {
    
    // UMTWRFS encoded
    daysOfWeek: sqlz.STRING,
    // 24 hour encoded
    startTime: sqlz.INTEGER,
    endTime: sqlz.INTEGER,
    // Just dates (YYYY-MM-DD)
    startDate: sqlz.DATE,
    endDate: sqlz.DATE
    
  });
  
}