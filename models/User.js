let crypto = require('crypto');
let sqlz = require('sequelize');

module.exports = (models, db) => {

  models.User = db.define('user', {
    username: {
      type: sqlz.STRING,
      unique: true
    },
    password: {
      type: sqlz.STRING
    },
    active: {
      type: sqlz.BOOLEAN,
      defaultValue: true
    },

    // XXX really poor permission overriding
    roleMedia: {
      type: sqlz.BOOLEAN,
      defaultValue: false,
      get() { return this.getDataValue('roleMedia') || this.username === 'admin' }
    },
    rolePlayer: {
      type: sqlz.BOOLEAN,
      defaultValue: false,
      get() { return this.getDataValue('rolePlayer') || this.username === 'admin' }
    },
    roleLayout: {
      type: sqlz.BOOLEAN,
      defaultValue: false,
      get() { return this.getDataValue('roleLayout') || this.username === 'admin' }
    },
    roleEditUsers: {
      type: sqlz.BOOLEAN,
      defaultValue: false,
      get() { return this.getDataValue('roleEditUsers') || this.username === 'admin' }
    }
  });

  models.User.prototype.setPassword = function(pwd) {
    // Do we really need this???
    return createHash(pwd).then(record => {
      this.setDataValue('password', record);
    });
  }

  models.User.prototype.checkPassword = function(pwd) {
    let record = this.getDataValue('password');
    return checkHash(pwd, record);
  }

  models.Session.belongsTo(models.User);
  
}

// Utilities for passwords
function createHash(password) {
  let salt = crypto.randomBytes(32);
  return new Promise((resolve, reject) => {
    crypto.pbkdf2(password, salt, 100000, 64, 'sha512', (err, key) => {
      if (err) reject(err);
      else resolve(salt.toString('base64') + ':' + key.toString('base64'));
    })
  })
}

function checkHash(password, record) {
  console.assert(/^[a-zA-Z0-9\/\+=]+:[a-zA-Z0-9\/\+=]+$/.test(record), 'Invalid stored password');
  
  let [salt, hash] = record.split(':').map(b => new Buffer(b, 'base64'));
  return new Promise((resolve, reject) => {
    crypto.pbkdf2(password, salt, 100000, 64, 'sha512', (err, key) => {
      if (err) reject(err);
      else resolve(key.equals(hash));
    })
  })
}