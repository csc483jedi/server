let sqlz = require('sequelize');
let Promise = require('bluebird');

module.exports = function(models, db) {

  let Layout = models.Layout = db.define('Layout', {
    id: {
      primaryKey: true,
      type: sqlz.INTEGER,
      autoIncrement: true
    },

    name: sqlz.STRING

  }, {
    hooks: {
      afterCreate: async function(layout, options) {
        // Make our root
        let root = await models.LayoutNode.create({
          nid: 1,
          name: layout.name,
          type: 'root'
        });

        await layout.setRoot(root);
        await root.setRootLayout(layout);
        await layout.addChild(root);
      }
    }
  })

  Layout.prototype.calcDuration = async function() {
    // TODO go to children and see how long we run
    return 30;
  }

  Layout.prototype.buildMetadata = async function() {
    // Create metadata

    let nodes = await this.getChildren({
      include: models.LayoutNode.Props
    });
    let nodeMap = {};
    let root;
    
    let files = [];

    // A little preprocessing
    for (let node of nodes) {
      node = nodeMap[node.id] = node.toJSON()//{};
      //for (let k in node) nodeMap[node.nid][k] = node[k];
      node.children = [];
      node.properties = {};
      for (let prop of node.props)
        node.properties[prop.key] = JSON.parse(prop.value);
      delete node.props;

      if (node.nid == 1)
        root = node;
    }

    let lines = [];

    // Re-childify
    for (let node of Object.values(nodeMap)) {
      if (node.parentId)
        nodeMap[node.parentId].children.push(node);
    }

    // Our processor
    let visit = async (node, parent = null) => {
      let {format} = require('util');
      
      let ret = [];
      let postLines = [];
      let styleList = [];
      
      // Some styles
      if (node.properties.hasOwnProperty('spaceShare')) {
        // Set flex to no grow, no shrink, auto basis (content-based?)
        styleList.push(format('flex: %s %s auto', node.properties.spaceShare, node.properties.spaceShare));
        
        // Have to reset height, width (in correct direction) if we don't actually want it to grow
        if (node.properties.spaceShare === 0 && parent && parent.type === 'divHorizon') {
          styleList.push('width: fit-content');
          
        }
        
        if (node.properties.spaceShare === 0 && parent && parent.type === 'divVertical') {
          styleList.push('height: fit-content');
        }
        
      }
        
      
      let styleString = styleList.length ? 'style="' + styleList.join('; ') + '"' : '';
      
      
      switch (node.type) {
        case 'root':
          // Just a div
          let minDuration = node.properties.duration || 0;
          ret.push('<div class="layout" data-min-duration="' + minDuration + '">')
          postLines.push('</div>');
          break;
        case 'divVertical':
        case 'divHorizon':
          // Also, both just divs with a class
          let divClass = node.type === 'divHorizon' ? 'horizontal' : 'vertical';
          ret.push(format('<div class="container %s" %s>', divClass, styleString));
          postLines.push('</div>');
          break;
        case 'weather':
          // Slightly odd, but still just a div!
          ret = ret.concat(format(`
<div class="content plugin" %s
 data-plugin="OpenWeatherMap"
 data-apiKey="f0e11f8c36c7ab877b2d5e2c38be9020"
 data-location="Flint,US"
 data-update="1h"></div>`, styleString).trim().split(/\r?\n/));
          break;
        case 'clock':
          // And, another div...
          ret.push(format('<div class="content plugin" data-plugin="Clock" %s>TIME TO GO</div>', styleString));
          break;
        case 'playlist':
          // Finally, something we can, put in another div
          ret.push(format('<div class="content auto playlist" %s>', styleString));
          postLines.push('</div>')
          break;
        case 'media':
          // Well, its an img at least
          let mediaClass = node.properties.scaling || 'distort';
          let {mediaItemId} = node.properties;
          let mediaItem = await models.MediaItem.findById(mediaItemId);
          
          if (!mediaItem) {
            throw new Error('Cannot find specified media item: ' + mediaItemId + ' on ' + node.nid);
          }
          
          let src = mediaItem.filepath;
          // Record that we need to upload this file later
          files.push(src);
          
          // If we're in a playlist, wrap
          if (parent.type === 'playlist') {
            // Duration priority order:
            //    PlaylistItem, Playlist, MediaItem, 0:10
            let rawDuration = node.properties.duration ||
                              parent.properties.defaultDuration ||
                              mediaItem.defaultDuration ||
                              10;

            console.log(node.name, rawDuration);
            
            let duration = Math.floor(rawDuration / 60) + ':' + (rawDuration % 60);
            
            ret.push(format('<div class="playlist-item" %s data-duration="%s">', styleString, duration))
            ret.push(format('  <img class="%s" src="/%s">', mediaClass, src))
            ret.push(       '</div>')
          } else {
            ret.push(       '<div class="content auto">')
            ret.push(format('  <img class="%s" src="/%s" %s>', mediaClass, src, styleString))
            ret.push(       '</div>')
          }
          
            
          break;
        default:
          throw new Error('NotImplemented ' + node.type + '.build');
      }

      // Visit children
      let inner = (await Promise.map(node.children, c => visit(c, node)))
        .reduce((acc, at) => acc.concat(at), [])
        .map(l => '  ' + l);

      // Put it all together
      ret = ret.concat(inner).concat(postLines);
      return ret;
    }

    return {
      body: (await visit(root)).map(l => '      ' + l).join('\n'),
      files
    }

    console.log(nodeMap); 
    console.log(JSON.stringify(root, null, 2));
    return JSON.stringify(root, null, 2);
  }  
}