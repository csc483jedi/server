let sqlz = require('sequelize');
let Promise = require('bluebird');

module.exports = function(models, db) {
  
  models.MediaItemFolder = db.define('MediaItemFolder', {
    id: {
      primaryKey: true,
      type: sqlz.INTEGER,
      autoIncrement: true
    },
    name: sqlz.STRING
  })

  models.MediaItemFolder.prototype.toTreeInfo = function() {
    return {
      type: 'folder',
      id: this.id,
      name: this.name, 
      parent: this.parentId
    }
  }
  
  models.MediaItemFolder.prototype.buildTree = async function() {
    let out = {
      type: 'folder',
      id: this.id,
      name: this.name, 
      children: []
    };
    
    let subFolders = await this.getSubFolders();
    out.children = out.children.concat(await Promise.map(subFolders, async sub => {
      return await sub.buildTree();
    }))
    
    let items = await this.getItems({ where: { status: 'confirmed'}});
    out.children = out.children.concat(await Promise.map(items, async item => {
      return await item.toTreeInfo();
    }))
    
    return out;
  }
  
  models.MediaItemFolder.prototype.delete = async function(recursive) {
    if (this.id === 1)
      throw new Error('Cannot delete root');
    
    if (recursive) {
      // Delete subfolders and children
      for (let subFolder of await this.getSubFolders()) {
        console.log('rm -r %s', subFolder.id);
        await subFolder.delete(recursive);
      }
      
      for (let item of await this.getItems({ where: { status: ['pending', 'confirmed'] } })) {
        console.log('rm %s', item.id);
        await item.delete();
      }
    } else {
      // Move everything to our parent
      let parent = await this.getParent();
      
      for (let subFolder of await this.getSubFolders()) {
        console.log('mv %s %s', subFolder.id, parent.id);
        await parent.addSubFolder(subFolder);
        await subFolder.setParent(parent);
      }
      
      for (let item of await this.getItems({ where: { status: ['pending', 'confirmed'] } })) {
        console.log('mv %s %s', item.id, parent.id);
        await parent.addItem(item);
        await item.setParent(parent);
      }
    }
    
    await this.destroy();
  }
  
  models.MediaItemFolder.hasMany(models.MediaItem, { as: 'items' });
  models.MediaItemFolder.belongsTo(models.MediaItemFolder, { as: 'parent' });
  models.MediaItem.belongsTo(models.MediaItemFolder, { as: 'parent' });
  models.MediaItemFolder.hasMany(models.MediaItemFolder, { as: 'subFolders' });
}