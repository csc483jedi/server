let express = require('express');
let {
  db,
  Layout,
  LayoutNode,
  Player,
  Playlist,
  PlaylistItem,
  MediaItem
} = require('../../models');
let Promise = require('bluebird');
let apiLayout = module.exports = express.Router();
let sqlz = require('sequelize');

// Root is /api/layout

apiLayout.get('/', async (req, res, next) => {
  // Return a list of layouts, with info on which players they are used in
  let layouts = await Layout.findAll();


  let out = [];

  for (let layout of layouts) {
    let obj = layout.get();

    let usedIn = (await layout.getPlayers()).map(p => p.name);

    if (usedIn.length === 0)
      obj.usedIn = 'None';
    else if (usedIn.length < 4)
      obj.usedIn = usedIn.join(', ');
    else
      obj.usedIn = usedIn.slice(0, 3).join(', ') + (usedIn.length - 3) + ' more'

    out.push(obj);
  }

  res.json(layouts);
});

apiLayout.post('/create', async (req, res, next) => {
  // Create a new, empty, layout and return id

  try {
    let name = req.body.name;

    if (!name) {
      res.status(400).json({
        error: 'InvalidRequest',
        message: 'Must specify name'
      })
      return;
    }

    let layout = await Layout.create({
      name
    })

    res.json({
      message: 'OK',
      id: layout.id
    });
  } catch (e) {
    console.log(e.stack);
    
    res.status(500).json({
      error: 'UnhandledError',
      message: 'Something went wrong'
    })
  }
});

apiLayout.use('/:id', async (req, res, next) => {
  // Load layout for subsequent routes
  let id = req.params.id;
  req.layout = await Layout.findOne({ where: { id }});
  
  if (!req.layout) {
    res.status(404).json({
      error: 'LayoutNotFound',
      message: 'Layout with specified id was not found'
    })
    
    return;
  }
  
  next();
})

apiLayout.get('/:id', async (req, res, next) => {
  // Specified layout
  let {layout} = req;

  // Get all our nodes
  let nodes = await layout.getChildren();
  let nodesOut = await Promise.map(nodes, node => node.toTreeInfo());

  let o = {};

  for (let node of nodesOut)
    o[node.id] = node;

  res.json(o)
});

apiLayout.post('/:id/save', async (req, res, next) => {
  // Save a layout
  // Process: delete everything that was and recreate it all
  
  let timer = new (require('../../Timer'))
  
  try {
    // Specified layout
    let {layout} = req;
    
    console.log('SAVING %sms', timer());
    
    // In a transaction in case something goes wrong (don't want half the layout left)
    await db.transaction(async (transaction) => {

      console.log('TRANSACT %sms', timer());
      
      // Update our name
      layout.name = req.body[1].name;
      await layout.save();
      
      // Delete all the old cruft
      let oldNodes = await layout.getChildren({ transaction });
      await Promise.map(oldNodes, node => node.destroy({ transaction }));
      
      
      console.log('KILLEDEMALL %sms', timer())
      
      let nodes = {};
      
      // Create the new cruft
      for (let nid in req.body) {
        
        let proto = req.body[nid];
        let node = await LayoutNode.create({
          nid,
          type: proto.type,
          name: proto.name,
          props: Object.entries(proto.properties || {})
                       .map((([key, value]) => ({key, value:JSON.stringify(value)})))
        }, {
          include: [{
            association: LayoutNode.Props
          }],
          transaction
        });
        
        /*console.log({
          nid,
          type: proto.type,
          name: proto.name,
          props: Object.entries(proto.properties || {})
                       .map((([key, value]) => ({key, value:JSON.stringify(value)})))
        });*/
        
        await layout.addChild(node, {transaction});
        await node.setRootLayout(layout, {transaction});
        nodes[nid] = node;
      }
      
      console.log('MADEEMALL %sms', timer());
      
      // Link up the new tree
      
      await layout.setRoot(nodes[1], {transaction});
      
      for (let nid in req.body) {
        if (!req.body[nid].parent) continue;
        
        //console.log('Linking %s to %s', nid, req.body[nid].parent);
        
        let parent = nodes[req.body[nid].parent];
        await parent.addChild(nodes[nid], {transaction});
        await nodes[nid].setParent(parent, {transaction});
      }
      
      console.log('LINKEDEMALL %sms', timer())
      
      // Probably fine
      
      console.log('KOSHER %sms', timer());
    })
    
    res.json({
      message: 'maybe ok'
    })
  } catch (e) {
    if (typeof transaction !== 'undefined')
      await transaction.rollback();
    
    console.error(e.stack);
    
    res.status(500).json({
      error: 'UnhandledError',
      message: 'Something went horribly wrong'
    })
  }
})

apiLayout.get('/:id/build', async (req, res, next) => {
  // Build just this layout
  let {layout} = req;
  
  let info = await layout.buildMetadata();
  res.render('playerTemplate', { layouts: [info] })
})

apiLayout.post('/:id/deployAll', async (req, res, next) => {
  // Deploy all signs with this layout
  let {layout} = req;
  
  let players = await layout.getPlayers();
  
  let tasks = [];
  
  for (let player of players) {
    tasks.push(player.deploy());
  }
  
  await Promise.all(tasks);
  
  res.json({
    message: 'OK'
  })
})

apiLayout.delete('/:id', async (req, res, next) => {
  // Delete a layout
  let {layout} = req;
  
  // XXX TODO add beforeDestroy hook to delete children, currently leaves orphans
  await layout.destroy();
  
  res.json({
    message: 'OK'
  })
})