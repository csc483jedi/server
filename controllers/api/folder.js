let express = require('express');
let util = require('util');
let Promise = require('bluebird');
let apiFolder = module.exports = express.Router();
let {MediaItem, MediaItemFolder} = require('../../models');

// Root is /api/folder

apiFolder.post('/create', async (req, res, next) => {
  let name = req.body.name;
  
  if (!name) {
    res.status(400).json({
      error: 'InvalidName',
      message: 'Must specify folder name'
    })
    return;
  }

  let parent = await MediaItemFolder.findOne({
    where: { id: req.body.parent }});
  
  if (!parent) {
    res.status(404).json({
      error: 'FolderNotFound',
      message: 'Folder with specified id was not found'
    })
    
    return;
  }
  
  let child = await MediaItemFolder.create({
    name
  });
  
  await parent.addSubFolder(child);
  await child.setParent(parent);
  
  res.json({
    message: 'OK',
    id: child.id
  })
});


apiFolder.post('/:id/move', async (req, res, next) => {
  // Move MediaItem to new folder

  let item = await MediaItemFolder.findOne({ where: { id: req.params.id }});

  if (!item) {
    res.status(404).json({
      error: 'MediaFolderNotFound',
      message: 'Specified Folder not found with id ' + req.params.id
    });
    return;
  }

  let to = req.body.to;

  let newParent = await MediaItemFolder.findOne({ where: { id: to }});

  if (!newParent) {
    res.status(404).json({
      error: 'MediaFolderNotFound',
      message: 'Specified Folder not found with id ' + to
    });
    return;
  }

  // XXX we assume we had a parent before
  let oldParent = await item.getParent();

  // Do the shuffle
  // TODO put this in a transaction, perhaps?
  await oldParent.removeSubFolder(item);
  await newParent.addSubFolder(item);
  await item.setParent(newParent);

  res.json({
    message: 'OK'
  })
})

apiFolder.delete('/:id/:recursive?', async (req, res, next) => {
  let id = req.params.id
  let recursive = !!req.params.recursive
  
  let folder = await MediaItemFolder.findOne({ where: { id }});
  
  if (!folder) {
    res.status(404).json({
      error: 'FolderNotFound',
      message: 'Folder with specified id was not found'
    });
    
    return;
  }
  
  try {
    await folder.delete(recursive);
  } catch (e) {
    res.status(500).json({
      error: 'UnhandledError',
      message: e.message
    });
    
    throw e;
  }
  
  res.json({
    message: 'OK'
  })
})