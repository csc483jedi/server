let express = require('express');
let multer = require('multer')
let util = require('util');
let Promise = require('bluebird');
let fs = require('fs');
Promise.promisifyAll(fs);
let apiMedia = module.exports = express.Router();
let {MediaItem, MediaItemFolder, Schedule} = require('../../models');

// Root is /api/media

apiMedia.get('/', async (req, res, next) => {
  // Listing of MediaItem and MediaItemFolder, linearly
  // Only contains basic info
 
  let folderList = (await MediaItemFolder.findAll()).map(f => f.toTreeInfo());
  let itemList = (await MediaItem.findAll({
    where: { 'status': 'confirmed' }
  })).map(i => i.toTreeInfo())

  let folders = {};
  let items = {};
  folderList.forEach(f => folders[f.id] = f);
  itemList.forEach(i => items[i.id] = i);

  res.json({ folders, items })
})

apiMedia.get('/pending', async (req, res, next) => {
  // Send a list of pending uploads
  let pending = await MediaItem.findAll({
    where: { status: 'pending' }
  });
  
  res.json(await Promise.map(pending, async pending => {
    let parent = await pending.getParent();
    
    return {
      id: pending.id,
      name: pending.name,
      originalName: pending.originalName,
      filesize: pending.filesize,
      destination: parent ? parent.name : 'Root',
      // XXX bad hack
      progress: 100
    }
  }))
})

apiMedia.get('/:id', async (req, res, next) => {
  // List full info for a specific MediaItem
  // including schedule, appears in info

  let item = await MediaItem.findOne({ where: { id: req.params.id }});

  if (!item) {
    res.status(404).json({
      error: 'MediaNotFound',
      message: 'Media Item not found with id ' + req.params.id
    });
    return;
  }

  // TODO schedule
  let defaultSchedule = await item.getDefaultSchedule();

  res.json({
    item,
    defaultSchedule
  })
})

apiMedia.post('/:id/move', async (req, res, next) => {
  // Move MediaItem to new folder
  let item = await MediaItem.findOne({ where: { id: req.params.id }});

  if (!item) {
    res.status(404).json({
      error: 'MediaNotFound',
      message: 'Media Item not found with id ' + req.params.id
    });
    return;
  }

  let to = req.body.to;

  let newParent = await MediaItemFolder.findOne({ where: { id: to }});

  if (!newParent) {
    res.status(404).json({
      error: 'MediaFolderNotFound',
      message: 'Specified Folder not found with id ' + to
    });
    return;
  }

  let oldParent = await item.getParent();

  await oldParent.removeItem(item);
  await newParent.addItem(item);
  await item.setParent(newParent);

  res.json({
    message: 'OK'
  })
})

let upload = multer({ dest: 'media/temp/' })

apiMedia.post('/upload', upload.single('file'), async (req, res, next) => {
  // Handles the uploading of a new media file
  // Creates db entry, receives from multer, saves to /media folder
  // Also ensures that we have a legitimate media file
  
  async function removeTempFile() {
    // Remove our temp file
    await fs.unlinkAsync(req.file.path);
  }
  
  // Ensure we have an image file
  if (!req.file.mimetype.startsWith('image')) {
    res.status(415).json({
      error: 'InvalidMediaFiletype',
      message: util.format('The uploaded file is of type %s, which is unacceptable',
        req.file.mimetype)
    })
    await removeTempFile();
    return;
  }
  
  
  
  let parentId = (req.body && req.body.parent) || 1;
  
  let parent = await MediaItemFolder.findOne({ where: { id: parentId }});
  
  if (!parent) {
    res.status(404).json({
      error: 'InvalidParent',
      message: 'The folder specified (or root) does not exist'
    })
    await removeTempFile();
    return;
  }
  
  let item = await MediaItem.create({
    name: req.file.originalname,
    originalName: req.file.originalname,
    filepath: req.file.path,
    filesize: req.file.size
  });
  
  await parent.addItem(item);
  await item.setParent(parent);
  
  // All hunky dory
  res.status(200).json({
    message: 'OK',
    id: item.id
  })
})



apiMedia.post('/:id', async (req, res, next) => {
  // Updates media info
  // Sets state to confirmed or rejected (but never pending) (???)
  // If pending before, moves from /temp to /media if confirmed, deletes otherwise
  
  let {item: itemData, schedule: scheduleData} = req.body;
  
  console.log(req.body);
  
  let id = parseInt(req.params.id)
  
  // Ensure we are the right item
  if (id !== itemData.id) {
    res.status(400).json({
      error: 'IDMismatch',
      message: 'The ID specified in the URL and the body do not match'
    })
    
    return;
  }
  
  let item = await MediaItem.findOne({ where: { id }});
  
  // Update these fields
  let fields = [
    'name',
    'originalName',
    'defaultDuration'
  ];
  
  for (let field of fields) {
    item[field] = itemData[field];
  }
  
  // Throw out old schedule, use new schedule
  let oldSchedule = await item.getDefaultSchedule();
  if (oldSchedule)
    await oldSchedule.destroy();
  
  if (scheduleData) {
    console.log('Making schedule');
    let newSchedule = await Schedule.create(scheduleData);
    
    await item.setDefaultSchedule(newSchedule);
  }
  
  
  // Update pending to not
  if (item.status === 'pending') {
    item.status = 'confirmed';
    
    // Move the file around
    let newFilePath = item.filepath.replace('temp/', '');
    await fs.renameAsync(item.filepath, newFilePath);
    
    item.filepath = newFilePath;
  }
  
  await item.save();
  
  res.json({
    message: 'OK'
  })
})

apiMedia.delete('/:id', async (req, res, next) => {
  // Delete media item
  // Sets state to deleted
  // Current state must be in [confirmed, pending]
  
  let item = await MediaItem.findOne({ where: { id: req.params.id }});
  
  if (!item) {
    res.status(404).json({
      error: 'MediaNotFound',
      message: 'Media Item not found with id ' + req.params.id
    });
    
    return;
  }
  
  await item.delete();
  
  
  
  res.json({
    message: 'OK'
  });
});