let express = require('express');
let apiPlayer = module.exports = express.Router();
let path = require('path');
let nunjucks = require('nunjucks');
let Promise = require('bluebird');
let SSH = require('node-ssh');
let {Player, Layout} = require('../../models');

// Root is /api/player

apiPlayer.get('/', async (req, res, next) => {
  // Get a list of players with some info
  let playerMap = {};

  let players = await Promise.map(Player.findAll(), async player => {
    playerMap[player.id] = {
      id: player.id,
      name: player.name,
      hostname: player.hostname,
      lastStatus: player.lastStatus,
      publicKey: player.publicKey
    }
  });
  
  res.json(playerMap);
});

apiPlayer.get('/status', async (req, res, next) => {
  // Get the statuses for all the players
  let statusMap = {};

  let players = await Promise.map(Player.findAll(), async player => {
    let dt = (Date.now() / 1000) - (player.lastStatusTime || 0);
    
    // 5 minutes?
    if (dt > 60 * 5*0) {
      try {
        console.log('Checking status of Player<%s>', player.hostname);
        
        let test = new SSH();
        await test.connect({
          host: player.hostname,
          username: 'signage',
          privateKey: player.getPrivateKey(),
          readyTimeout: 5000
        })
        
        
        player.lastStatus = 'active';
      } catch (e) {
        if (e.code === 'ECONNREFUSED') {
          player.lastStatus = 'refused';
        } else if (e.code/* === 'EHOSTUNREACH'*/) { // Just lump other codes here
          player.lastStatus = 'unreachable'
        } else if (e.message = 'All configured authentication methods failed') {
          player.lastStatus = 'badLogin';
        } else {
          // Something else
          player.lastStatus = 'offline';
        }
        
        console.log('Failed with Player<%s>: ', player.hostname, e.message);
      }
      
      player.lastStatusTime = Date.now() / 1000;
      await player.save();
    }
    
    statusMap[player.id] = player.lastStatus;
  });

  res.json(statusMap);
})

apiPlayer.post('/create', async (req, res, next) => {
  // Create a new player
  let player = await Player.create({
    name: 'Unnamed player',
    hostname: 'your-player.businessname.website'
  });
  
  // Set a key first thing
  await player.rekey();
  
  res.json({
    message: 'OK',
    id: player.id
  })
});

apiPlayer.use('/:id', async (req, res, next) => {
  // Fetch the specified player to req.player for the subsequent handlers
  let id = req.params.id;
  req.player = await Player.findById(id)
  
  if (!req.player) {
    res.status(404).json({
      error: 'PlayerNotFound',
      message: 'Player with the specified id was not found'
    });
    
    return;
  }
  
  next();
})

apiPlayer.post('/:id/rekey', async (req, res, next) => {
  // Genrate a new keypair for this player
  let player = {req}
  
  await player.rekey();
  
  res.json({
    message: 'OK',
    publicKey: player.publicKey
  })
});

apiPlayer.get('/:id/build', async (req, res, next) => {
  // Build html for the player
  let {player} = req;

  try {
    //res.send('<xmp>' + (await player.build()).body);
    let layouts = await player.build();
    console.log(layouts)
    res.render('playerTemplate', { layouts })
  } catch (e) {
    res.status(500).send('<pre>' + e.stack);
  }
})

apiPlayer.get('/:id', async (req, res, next) => {
  // Get info for a specific player
  let {player} = req

  let layouts = await Promise.map(await player.getLayouts(), l => l.id);
  
  res.json({
    player,
    layouts
  });
})

apiPlayer.post('/:id', async (req, res, next) => {
  // Set info for a specific player
  try {
    let {player} = req

    player.name = req.body.name;
    player.hostname = req.body.hostname;

    let layouts = await Promise.map(req.body.layouts, id => Layout.findById(id));

    await player.setLayouts(layouts);

    await player.save();

    res.json({
      message: 'OK'
    })
  } catch (e) {
    console.error(e.stack);
    res.status(500).json({
      error: 'UnhandledError',
      message: 'Unexpected Error'
    })
  }
});

apiPlayer.get('/:id/status', async (req, res, next) => {
  // Get status for this player (XXX TEST)
  try {
    let {player} = req
    
    let session = new SSH();
    
    await session.connect({
      host: player.hostname,
      username: 'signage',
      privateKey: player.getPrivateKey()
    })
    
    console.log('ok');
    
    let {stdout, stderr} = await session.execCommand('cat cookie');
    
    res.end(stdout);
  } catch (e) {
    console.error(e.stack);
    res.status(500).json({
      error: 'UnhandledError',
      message: 'Unexpected Error'
    })
  }
})

apiPlayer.post('/:id/deploy', async (req, res, next) => {
  // Deploy our files and tell the player to reload
  let {player} = req;
  
  try {
    await player.deploy();
  } catch (e) {
    console.error(e);
    res.status(500).json({
      error: 'DeployError',
      message: 'Deploy Failed'
    })
    return;
  }
  
  res.json({
    message: 'OK'
  })
});

apiPlayer.delete('/:id', async (req, res, next) => {
  // Delete the player
  let {player} = req;
  
  try {
    await player.destroy();
  } catch (e) {
    console.error(e);
    res.status(500).json({
      error: e.stack,
      message: 'Failed to delete the Player'
    })
    return;
  }
  
  res.json({
    message: 'OK'
  })
})