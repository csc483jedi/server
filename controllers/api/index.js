let express = require('express');
let api = module.exports = express.Router();

api.use('/media',  require('./media' ));
api.use('/user',   require('./user'  ));
api.use('/folder', require('./folder'));
api.use('/player', require('./player'));
api.use('/user',   require('./user'  ));
api.use('/layout', require('./layout'));

// Catch all JSON-friendly error handler
api.use((err, req, res, next) => {
  res.status(500).json({
    error: 'UnhandledError',
    message: 'Something went wrong: ' + err.message,
    err
  })
})