let express = require('express');
let apiUser = module.exports = express.Router();
let {User} = require('../../models');
let crypto = require('crypto');

// Root is /api/user

apiUser.get('/', async (req, res, next) => {
  // Send a list of users (sans password hashes)
  let users = await User.findAll({
    where: { active: true }
  });

  let out = JSON.stringify(users, (k, v) => {
    if (k === 'password') return;

    return v;
  });

  res.send(out);
})

apiUser.get('/:id', async (req, res, next) => {
  // Send specified user (sans password hash)
  let id = req.params.id;
  let user = await User.findOne({ where: { id }});

  if (!user) {
    res.status(404).json({
      error: 'UserNotFound',
      message: 'Specified user was not found'
    })
    return;
  }

  let out = JSON.stringify(user, (k, v) => {
    if (k === 'password') return;

    return v;
  });

  res.send(out);
});

apiUser.post('/create', async (req, res, next) => {
  // Create a new user with the username specified and a hogwash password
  let username = req.body.username;

  if (!username) {
    res.status(400).json({
      error: 'InvalidRequest',
      message: 'Must specify username'
    })
    return;
  }

  // Ensure they don't already exist
  let existing = await User.findOne({ where: { username }});
  if (existing) {
    res.status(409).json({
      error: 'AlreadyExistingUser',
      message: 'User with that name already exists'
    });
    return;
  }

  let password = crypto.randomBytes(16).toString('hex');

  let user = await User.create({
    username
  });

  await user.setPassword(password);
  await user.save();

  res.json({
    message: 'OK',
    id: user.id,
    password
  });
});

apiUser.post('/:id', async (req, res, next) => {
  // Update user permissions
  let id = req.params.id;
  let user = await User.findOne({ where: { id }});

  if (!user) {
    res.status(404).json({
      error: 'UserNotFound',
      message: 'Specified user was not found'
    })
    return;
  }

  // Set specified props
  // XXX slightly opaque and evil-ish
  let props = {
    roleMedia = user.roleMedia,
    rolePlayer = user.rolePlayer,
    roleLayout = user.roleLayout,
    roleEditUsers = user.roleEditUsers
  } = req.body;

  Object.assign(user, props);

  // Posting here also makes an account active
  user.active = true;

  await user.save();

  res.json({
    message: 'OK'
  })
});

apiUser.post('/:id/deactivate', async (req, res, next) => {
  // Deactivate user account
  let id = req.params.id;
  let user = await User.findOne({ where: { id }});

  if (!user) {
    res.status(404).json({
      error: 'UserNotFound',
      message: 'Specified user was not found'
    })
    return;
  }

  user.active = false;
  await user.save();

  res.json({
    message: 'OK'
  });
})


apiUser.post('/:id/password', async (req, res, next) => {
  // Update user password if admin or current user
  let id = req.params.id;
  let password = req.body.password;

  // Some simple silly checks
  if (!password || password.length < 8) {
    res.status(400).json({
      error: 'InvalidPassword',
      message: 'Password must be specified and of at least 8 characters'
    });
    return;
  }

  let user = await User.findOne({ where: { id }});

  if (!user) {
    res.status(404).json({
      error: 'UserNotFound',
      message: 'Specified user was not found'
    })
    return;
  }

  console.log('Setting to %s', password)
  await user.setPassword(password);
  await user.save();

  res.json({
    message: 'OK'
  })
});