let express = require('express');
let {Session, User} = require('../models');

let LoginController = module.exports = express.Router();

LoginController.get('/login', (req, res, next) => {
  res.render('login')
});

LoginController.post('/login', async (req, res, next) => {
  let user = await User.findOne({ where: { username: req.body.username }});
  if (user && await user.checkPassword(req.body.password)) {
    // Make a new session
    
    let session = await Session.create();
    await session.setUser(user);
    
    res.cookie('session', session.token);
    res.redirect('/');
  } else {
    res.render('login', {
      message: 'Invalid login'
    })
  }
  
});

LoginController.get('/logout', async (req, res, next) => {
  let token = req.cookies.session;
  let session = await Session.findOne({where: {token}});

  if (session)
    await session.destroy();

  res.clearCookie('token', undefined);

  res.redirect('/login');
})

// For all non-login routes, verify that the user has a valid token
LoginController.use(async (req, res, next) => {
  let token = req.cookies.session;
  let session = await Session.findOne({where: {token}});
  
  if (!session) {
    // No session by that token, redirect to login
    res.redirect('/login');
    return;
  }
  
  req.user = await session.getUser();
  
  next();
});