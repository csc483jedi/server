let express = require('express');
let router = module.exports = express.Router();

// Must be first to enforce authentication
router.use(require('./LoginController'));

// All API routes
router.use('/api', require('./api'));