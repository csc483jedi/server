module.exports = function Timer() {
  let start = Date.now();
  return (reset = true) => {
    let o = Date.now() - start;
    if (reset)
      start = Date.now();
    return o;
  }
}