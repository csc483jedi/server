module.exports = {
  // OK
  active: {
    text: 'Active',
    class: 'table-success',
    ok: true
  },
  sleep: {
    text: 'Sleep Mode',
    class: 'table-dark',
    ok: true
  },
  // Not OK
  unreachable: {
    text: 'Could not reach player, check address and network connection',
    class: 'table-danger',
    ok: false
  },
  refused: {
    text: 'Player refused to connect. Ensure correct address and check firewalls',
    class: 'table-danger',
    ok: false
  },
  badLogin: {
    text: 'Player refused key, copy the key to /home/signage/.ssh/authorized_keys',
    class: 'table-danger',
    ok: false
  },
  offline: {
    text: 'Player offline, unknown error',
    class: 'table-warning',
    ok: false
  }
}