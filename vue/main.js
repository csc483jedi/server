let Vue = require('vue/dist/vue.common');
let VueRouter = require('vue-router');
let VueResource = require('vue-resource');

Vue.use(VueRouter);
Vue.use(VueResource);

// Our components
let main = require('./main.vue');
Vue.component('jedi-nav', require('./components/nav.vue'));
Vue.component('jedi-tree', require('./components/tree.vue'));
Vue.component('jedi-schedule', require('./components/schedule.vue'));
Vue.component('jedi-dropzone', require('./components/dropzone.vue'));
Vue.component('jedi-help', require('./components/helpModal.vue'));
Vue.component('jedi-duration', require('./components/duration.vue'));
Vue.component('jedi-toggle', require('./components/toggle.vue'));

// Some 3rd-party components
//Vue.component('BootstrapToggle', require('vue-bootstrap-toggle'));

// The router
let routes = [
  { path: "/", redirect: '/players' },
  
  // Media
  { path: "/media", component: require('./media/mediaList.vue') },
  { path: "/media/pending", component: require('./media/pendingMedia.vue') },
  { path: "/media/:id", component: require('./media/mediaInfo.vue') },
  
  // Players
  { path: "/players", component: require('./player/playerList.vue') },
  { path: "/players/:id", component: require('./player/playerInfo.vue') },

  // Layouts
  { path: "/layouts", component: require('./layout/layoutList.vue') },
  { path: "/layouts/:id", component: require('./layout/layoutEditor.vue') },

  // Users
  { path: "/users", component: require('./user/userList.vue') },
  { path: "/users/:id", component: require('./user/userInfo.vue') }
];

let router = new VueRouter({
  routes,
  linkActiveClass: 'active'
})

// Main configuration
let App = window.app = new Vue({
  el: '#app',
  render: createElement => createElement(main),
  router
})